import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login-signup/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS }    from '@angular/common/http';
import { UserService } from './services/user.service';
import { HomePageComponent } from './components/home-page/home-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SignupComponent } from './components/login-signup/signup/signup.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MaterialModule } from './material';
import { LoginSignupComponent } from './components/login-signup/login-signup.component';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { UserSettingsComponent } from './components/user-settings/user-settings.component'
import { AuthenticationInterceptor } from './security/authentication.interceptor';
import { UserTableComponent } from './components/admin-page/user-table/user-table.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { UserInfoComponent } from './components/user-settings/user-info/user-info.component';
import { SecurityComponent } from './components/user-settings/security/security.component';
import { ConfirmAccountComponent } from './components/confirm-account/confirm-account.component';
import { ResetPasswordDialogComponent } from './components/modals/reset-password-dialog/reset-password-dialog.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ConfirmAccountDialogComponent } from './components/modals/confirm-account-dialog/confirm-account-dialog.component';
import { CreateAdminComponent } from './components/admin-page/create-admin/create-admin.component';
import { DeleteUserDialogComponent } from './components/modals/delete-user-dialog/delete-user-dialog.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomePageComponent,
    SignupComponent,
    LoginSignupComponent,
    AdminPageComponent,
    UserSettingsComponent,
    UserTableComponent,
    NavigationComponent,
    UserInfoComponent,
    SecurityComponent,
    ConfirmAccountComponent,
    ResetPasswordDialogComponent,
    ResetPasswordComponent,
    ConfirmAccountDialogComponent,
    CreateAdminComponent,
    DeleteUserDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    LayoutModule,
   
  ],
  entryComponents: [
    ResetPasswordDialogComponent,
    ConfirmAccountDialogComponent,
    DeleteUserDialogComponent
  ],
  providers: [UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
