export class Roles {
    public static admin = 'ROLE_ADMIN';
    public static user = 'ROLE_USER';
}
