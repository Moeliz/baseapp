export class User {
    
    id: number;
    firstName: string;
    surname: string;
    email: string;
    roles: string[] = [];
    password: string;
    newPassword: string;
}
