import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';


@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.match("api/")) {
      if(localStorage.getItem("jwt")){

        
        req = req.clone({
          setHeaders: {
            Authorization: localStorage.getItem("jwt")
          }
        })
      }
      return next.handle(req).pipe(
        map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            console.log('event--->>>', event);
          }
          return event;
        }),
        catchError((error: HttpErrorResponse) => {
          console.log(error)
          if (error.status == 401) {
            localStorage.removeItem("jwt")
            this.router.navigate(['/login']);
            this.snackBar.open("You have been logged out because your token is invalid","", {
              duration: 3000,
            })
          }
          
          
          return throwError(error);
        }));
      }
    }

}
