import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, CanDeactivate } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate
{

  constructor(private router: Router, public authService: AuthenticationService) {}
  canActivate(
    next: ActivatedRouteSnapshot) {
      if (this.authService.isAuthenticated()) {
        return true;
      } else {
        //TODO notify user why signed out or redirect to Not Authenticated view.
        this.router.navigate(['/login']);
        return false;
      }
  }
// TODO remove possibilty to login when already logged in
  // canDeactivate(component: LoginComponent, currentRoute: ActivatedRouteSnapshot) {
  //   return this.userService.isAuthenticated();
  // }
}
