import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class NoauthGuard implements CanActivate {
  constructor(private router: Router, public authService: AuthenticationService) { }
  canActivate(
    next: ActivatedRouteSnapshot) {
    if (!this.authService.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(['/homepage']);
      return false;
    }

  }
}
