import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(public authService: AuthenticationService, public router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot) {
      const expectedRole = next.data.expectedRole;
      if (!this.authService.hasRole(expectedRole)) {
        // TODO redirect to not authorized page
        // this.router.navigate(['/error']);
        return false;
      }
      if (!this.authService.isAuthenticated()) {
        this.router.navigate(['/login']);
        return false;
      }
      return true;
    }
  
}
