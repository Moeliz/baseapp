import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserToken } from '../models/user-token';

const API_URL = environment.apiUrl + "/user";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  jwtHelper = new JwtHelperService()

  constructor(
    private http: HttpClient,
    private router: Router

  ) { }

  login(user: User): Observable<HttpResponse<Object>> {
    localStorage.removeItem("jwt")
    return this.http.post<HttpResponse<Object>>(API_URL + "/login", user, { observe: 'response' }).pipe(
      tap(resp => {
        localStorage.setItem("jwt", "Bearer " + resp.headers.get('Authorization'))
      }
      ))
  }
  signOut() {
    localStorage.removeItem("jwt")
    this.router.navigate(['/login'])
  }

  getRoles() {
    let token = localStorage.getItem('jwt')
    try {
      if (this.jwtHelper.decodeToken(token) != null) {
        let decodedToken = this.jwtHelper.decodeToken(token)
        return decodedToken.auth
      } else {
        localStorage.removeItem('jwt')
        this.router.navigate(['/login'])
      }
    } catch (e) {
      localStorage.removeItem('jwt')
      this.router.navigate(['/login'])
    }
  }
  confirmAccount(token: string) {
    return this.http.get(API_URL + token, { observe: 'response' })
  }
  hasRole(roles: string[]) {
    let hasRole = false
    this.getRoles().forEach(role => {
      roles.forEach(rol => {
        if (role.authority == rol) {
          hasRole = true
        }
      })
    })
    return hasRole
  }
  public isAuthenticated(): boolean {
    try {
      let isExpired = this.jwtHelper.isTokenExpired(localStorage.getItem("jwt"))
      return !isExpired
    } catch (e) {
      return false;
    }

  }
  sendResetPasswordEmail(user: User) {
    return this.http.post(API_URL + "/resetpasswordemail", user)
  }
  resetPassword(userToken: UserToken) {
    return this.http.post(API_URL + "/resetpassword", userToken)
  }
}
