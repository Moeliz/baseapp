import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User } from '../models/user';
import { Observable} from 'rxjs';
import { tap } from 'rxjs/operators';




const API_URL = environment.apiUrl + "/user";

@Injectable({
  providedIn: 'root'
})
export class UserService {


  constructor(
    private http: HttpClient,

  ) { }

  createUser(user: User) {
    return this.http.post(API_URL + "/createuser", user)
  }

  createAdmin(user: User) {
    return this.http.post(API_URL + "/createadmin", user)
  }
  deleteUser(id: number) {
    return this.http.delete(API_URL + "/" + id)
  }

  updateUser(user: User): Observable<HttpResponse<Object>> {
    return this.http.put<HttpResponse<Object>>(API_URL, user, { observe: 'response' }).pipe(
      tap(resp => {
        localStorage.setItem("jwt", "Bearer " + resp.headers.get('Authorization'))
      }
      ));
  }
  updatePassword(user: User): Observable<HttpResponse<Object>> {
    return this.http.put<HttpResponse<Object>>(API_URL + "/password", user, { observe: 'response' }).pipe(
      tap(resp => {
        localStorage.setItem("jwt", "Bearer " + resp.headers.get('Authorization'))
      }
      ));
  }

  getUser() {
    return this.http.get(API_URL)
  }

  getAllUsers(pageInfo) {
    
    return this.http.get(API_URL + "/allusers/" + pageInfo.sortBy + "/" + pageInfo.direction + "/" + pageInfo.page + "?search=" + pageInfo.searchWords)
  }
}
