import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TabService {


  tabSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  emailSubject: BehaviorSubject<string> = new BehaviorSubject<string>("");

  constructor() { }

  setTabSelected(tabIndex: number) {
    this.tabSubject.next(tabIndex);
  }

  getTabSelected() {
    return this.tabSubject.asObservable();
  }
  setEmail(email: string) {
    this.emailSubject.next(email);
  }

  getEmail() {
    return this.emailSubject.asObservable();
  }
}
