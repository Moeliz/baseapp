import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmAccountDialogComponent } from './confirm-account-dialog.component';

describe('ConfirmAccountDialogComponent', () => {
  let component: ConfirmAccountDialogComponent;
  let fixture: ComponentFixture<ConfirmAccountDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmAccountDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmAccountDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
