import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-confirm-account-dialog',
  templateUrl: './confirm-account-dialog.component.html',
  styleUrls: ['./confirm-account-dialog.component.css']
})
export class ConfirmAccountDialogComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<ConfirmAccountDialogComponent>,
  ) { }

  ngOnInit() {
  }

}
