import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { emailValidator } from 'src/app/validators/validator';

@Component({
  selector: 'app-reset-password-dialog',
  templateUrl: './reset-password-dialog.component.html',
  styleUrls: ['./reset-password-dialog.component.css']
})
export class ResetPasswordDialogComponent implements OnInit {
  emailForm
  constructor(
    private dialogRef: MatDialogRef<ResetPasswordDialogComponent>,
    private formBuilder: FormBuilder,
  ) {
    this.emailForm = this.formBuilder.group({
      email: ["", emailValidator],
    })
  }

  ngOnInit() {
  }

  onSubmit(email) {
    this.dialogRef.close(email)
  }
  onClose() {
    this.dialogRef.close();
  }
}
