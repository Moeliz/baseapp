import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { timer } from 'rxjs';

@Component({
  selector: 'app-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.css']
})
export class ConfirmAccountComponent implements OnInit {

  isEnabled: boolean = undefined


  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private snackBar: MatSnackBar) { }


  ngOnInit() {
    const changePageTimer = timer(3000);
    this.authService.confirmAccount(this.router.url).subscribe(response => {
      if (response.status == 200) {
        this.isEnabled = true
        changePageTimer.subscribe(val => {
          this.router.navigate(['/login']);
          this.snackBar.open("Your account is verified, please log in", "", {
            duration: 3000,
          })
        })
      }
    }, (error) => {
      this.isEnabled = false
    })
  }
}


