import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { emailValidator } from '../../../validators/validator';


@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

  userInfoForm
  hasError = false
  user: User


  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private snackBar: MatSnackBar,
  ) {
    this.userInfoForm = this.formBuilder.group({
      email: ["", emailValidator],
      firstName: "",
      surname: "",
      password: ""
    })
  }

  ngOnInit() {
    this.userService.getUser().subscribe((user: User) => {
      this.user = user;
      this.userInfoForm = this.formBuilder.group({
        email: [this.user.email, emailValidator],
        firstName: this.user.firstName,
        surname: this.user.surname,
        password: ["", [Validators.required, Validators.minLength(8)]],
      })
    })
  }

  onSubmit(userData, focus) {
    let config = new MatSnackBarConfig()
    config.panelClass = ['snackbar']
    config.duration = 3000
    config.horizontalPosition = "right"
    this.userService.updateUser(userData).subscribe(() => {
      this.snackBar.open("Account updated", "", config)
    },
      error => {
        if (error.status == 404) {
          this.hasError = true;
          focus.focus();
          this.snackBar.open("Couldn't update account. Wrong password", "", config)
        }
        if (error.status == 409) {
          this.hasError = true;
          focus.focus();
          this.snackBar.open("Couldn't update account. Email already taken", "", config)
        }
        if (error.status != 404 && error.status != 409) {
          this.hasError = true;
          focus.focus();
          this.snackBar.open("Couldn't update account. Something went wrong", "", config)
        }
      }
    )
  }
}
