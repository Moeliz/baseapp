import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { passwordValidator } from 'src/app/validators/validator';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.css']
})
export class SecurityComponent implements OnInit {

  passwordForm
  hasError = false



  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private snackBar: MatSnackBar,
  ) {
    this.passwordForm = this.formBuilder.group({
      password: "",
      newPassword: ["", [Validators.required, Validators.minLength(8)]],
      confirmPassword: [undefined, passwordValidator('newPassword')],
    })
  }

  ngOnInit() {
  }

  onSubmit(userData) {
    let config = new MatSnackBarConfig();
    config.panelClass = ['snackbar'];
    config.duration = 3000;
    config.horizontalPosition = "right";
    this.userService.updatePassword(userData).subscribe(() => {
      this.snackBar.open("Password is updated", "", config)
      this.passwordForm.reset()
      this.passwordForm.controls["newPassword"].setErrors(null)
    },
      error => {
        if (error.status == 404) {
          this.hasError = true;
          this.snackBar.open("Couldn't update password. Old password is wrong", "", config)
        }
        if (error.status != 404) {
          this.hasError = true;
          this.snackBar.open("Couldn't update password. Somehting is wrong", "", config)
        }
      }
    )

  }
}
