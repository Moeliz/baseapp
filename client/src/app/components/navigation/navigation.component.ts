import { Component, ViewChildren, QueryList } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Roles } from 'src/app/models/roles';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

    isAuthenticated$: boolean= this.isAuthenticated();
  roles = Roles;

  @ViewChildren('drawer') sc: QueryList<MatSidenav>;
  
  constructor(private authService: AuthenticationService,private breakpointObserver: BreakpointObserver) {}
  

  ngOnInit() {
  }

  signOut(){
    this.authService.signOut();
  }
  isAuthenticated(){
    
    return this.authService.isAuthenticated();
  }
  hasRole(roles: any[]) {
    return this.authService.hasRole(roles);
  }
}
