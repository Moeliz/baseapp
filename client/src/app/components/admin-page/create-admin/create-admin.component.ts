import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { emailValidator, passwordValidator } from '../../../validators/validator';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar, MatSnackBarConfig, MatDialogRef, MatDialog } from '@angular/material';
import { TabService } from 'src/app/services/tab.service';
import { ConfirmAccountDialogComponent } from '../../modals/confirm-account-dialog/confirm-account-dialog.component';


@Component({
  selector: 'app-create-admin',
  templateUrl: './create-admin.component.html',
  styleUrls: ['./create-admin.component.css']
})
export class CreateAdminComponent implements OnInit {
  
    newAdminForm
    hasError: boolean = false
    isLoading: boolean = false
    emailVerificationDialogRef: MatDialogRef<ConfirmAccountDialogComponent>
  
  
  
    constructor(
      private formBuilder: FormBuilder,
      private userService: UserService,
      private snackBar: MatSnackBar,
      ) { 
      this.newAdminForm = this.formBuilder.group({
        email: ["", emailValidator],
        password: ["", [Validators.required, Validators.minLength(8)]],
        confirmPassword: [undefined, passwordValidator('password')],
        firstName: "",
        surname: ""
      })
    }
  
    ngOnInit() {
    }
  
    onSubmit(userData){
      this.isLoading = true
      let config = new MatSnackBarConfig()
      config.duration = 3000
      this.userService.createAdmin(userData).subscribe(result => {
        this.snackBar.open("A new admin is created!", "", config)
        this.isLoading = true
      },
        error => {
          if (error.status == 409) {
            this.hasError = true;
            this.snackBar.open("Couldn't create account. Email already taken", "", config)
          }
          if (error.status != 409) {
            this.hasError = true;
            this.snackBar.open("Couldn't create account. Something went wrong", "", config)
          }
          this.isLoading = false
        },
        () => {
          this.isLoading = false
        }
      )
  
  }
  
}
