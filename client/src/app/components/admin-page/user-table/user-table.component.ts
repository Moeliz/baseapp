import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { PageableListAmount } from 'src/app/models/pageable-list-amount';
import { DeleteUserDialogComponent } from '../../modals/delete-user-dialog/delete-user-dialog.component';
import { MatDialogRef, MatDialog } from '@angular/material';
import { filter, debounceTime, map, distinctUntilChanged } from 'rxjs/operators';
import { fromEvent } from 'rxjs';



@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {

  pageInfo = {
    sortBy: "email",
    page: "0",
    direction: "asc",
    searchWords: ""
  }
  amount: number
  users: User[]
  isLoading: boolean = true;

  displayedColumns: string[] = ['email', 'first_name', 'surname', 'roles', 'actions']
  dataSource: User[]

  deleteUserDialogRef: MatDialogRef<DeleteUserDialogComponent>

  @ViewChild('searchInput', { static: true }) searchInput: ElementRef

  constructor(
    private userService: UserService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getAllUsers(this.pageInfo)
    fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      map((event: any) => {
        return event.target.value;
      })
      // ,filter(res => res.length >= 2 || res.length == 0)
      , debounceTime(500)
      , distinctUntilChanged()
    ).subscribe((searchInput: string) => {
      console.log(searchInput)
      this.search(searchInput)

    }, (err) => {
      console.log('error', err);
    });
  }

  // TODO If error
  getAllUsers(pageInfo) {
    this.userService.getAllUsers(pageInfo).subscribe((data: PageableListAmount) => {
      this.amount = data.amount
      this.dataSource = data.objects
      this.isLoading = false;
    })
  }
  onPageChangeEvent(pageEvent) {
    this.pageInfo.page = pageEvent.pageIndex
    this.getAllUsers(this.pageInfo)
  }
  onSortEvent(sortEvent) {
    this.pageInfo.sortBy = sortEvent.active
    if (sortEvent.direction !== "") {
      this.pageInfo.direction = sortEvent.direction
    } else {
      this.pageInfo.sortBy = "email"
      this.pageInfo.direction = "asc"
    }
    this.getAllUsers(this.pageInfo)

  }
  deleteUser(id: number) {
    console.log(id)
    this.userService.deleteUser(id).subscribe(() => {
      this.getAllUsers(this.pageInfo)
    })

  }
  openDeleteUserDialog(user) {
    this.deleteUserDialogRef = this.dialog.open(DeleteUserDialogComponent, {
      data: user
    })
    this.deleteUserDialogRef.afterClosed().pipe(filter(id => id)).subscribe(result => {
      this.deleteUser(result)

    })
  }
  search(searchWords) {
    this.pageInfo.searchWords = searchWords
    this.getAllUsers(this.pageInfo)

  }

}

