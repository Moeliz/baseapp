import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { passwordValidator } from 'src/app/validators/validator';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  resetPasswordForm: any
  resetToken


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private snackBar: MatSnackBar
  ) {
    this.resetPasswordForm = this.formBuilder.group(
      {
        token: [],
        password: ["", [Validators.required, Validators.minLength(8)]],
        confirmPassword: [undefined, passwordValidator('password')]
      })
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.resetToken = params.token
      this.resetPasswordForm = this.formBuilder.group(
        {
          token: [this.resetToken],
          password: ["", [Validators.required, Validators.minLength(8)]],
          confirmPassword: [undefined, passwordValidator('password')]
        })
    })
  }

  onSubmit(formData) {
    let config = new MatSnackBarConfig()
    config.duration = 2000
    this.authService.resetPassword(formData).subscribe(() => {
      this.router.navigate(["/login"])
      this.snackBar.open("Your password is saved, please log in", "", config)
    },
      error => {
        if (error.status != 200) {
          this.snackBar.open("Your reset password token is invalid", "", {
            duration: 2000,
          })

        }
      })

  }

}
