import { Component, OnInit } from '@angular/core';
import { TabService } from 'src/app/services/tab.service';
import { MatTabChangeEvent } from '@angular/material';

@Component({
  selector: 'app-login-signup',
  templateUrl: './login-signup.component.html',
  styleUrls: ['./login-signup.component.css']
})
export class LoginSignupComponent implements OnInit {

  selectedIndex : number

  constructor(private tabService: TabService) { }

  ngOnInit() {
      this.tabService.getTabSelected().subscribe((tabIndex : number) =>{
    this.selectedIndex = tabIndex
   })
  }
  onTabClick(event: MatTabChangeEvent) {
    this.tabService.setTabSelected(event.index)
  }



}

