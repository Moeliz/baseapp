import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar, MatDialog, MatDialogRef } from "@angular/material";
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TabService } from 'src/app/services/tab.service';
import { Router } from '@angular/router';
import { ResetPasswordDialogComponent } from '../../modals/reset-password-dialog/reset-password-dialog.component';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userForm: any
  hasError: boolean = true
  resetPasswordDialogRef: MatDialogRef<ResetPasswordDialogComponent>


  constructor(
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private tabService: TabService,
    private router: Router,
    private dialog: MatDialog
  ) {
    this.userForm = this.formBuilder.group({
      email: [""],
      password: [""],

    });

  }

  ngOnInit() {
    this.tabService.getEmail().subscribe((email: string) => {
      this.userForm.patchValue({
        email: email,
        password: [""],
      })
    })
  }

  onSubmit(userData, el) {
    this.authService.login(userData).subscribe(() => {
      this.router.navigate(['/homepage']);
      this.snackBar.open("Logged in", "", {
        duration: 3000,
      })
    },
      error => {
        if (error.status != 200) {
          let snackBarError = this.snackBar.open("Wrong email or password", "Try again", {
            duration: 3000,
          })
          el.focus();
          snackBarError.onAction().subscribe(() => {
            el.focus();
          });
        }
      }
    )
  }
  openResetPasswordDialog() {
    this.resetPasswordDialogRef = this.dialog.open(ResetPasswordDialogComponent)
    this.resetPasswordDialogRef.afterClosed().pipe(filter(email => email)).subscribe(email => {
      let user = new User()
      user.email = email
      this.authService.sendResetPasswordEmail(user).subscribe(()=>{
        this.snackBar.open("Reset password link sent to email sucessfully.", "", {
          duration: 3000,
        })
      },error =>{
        if (error.status == 404) {
          this.snackBar.open("No account with email " + email + " was found", "", {
            duration: 3000,
          })
        }
      })
    })
  }
}
