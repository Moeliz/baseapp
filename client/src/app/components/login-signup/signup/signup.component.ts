import { Component, OnInit} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { emailValidator, passwordValidator } from '../../../validators/validator';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar, MatSnackBarConfig, MatDialogRef, MatDialog } from '@angular/material';
import { TabService } from 'src/app/services/tab.service';
import { ConfirmAccountDialogComponent } from '../../modals/confirm-account-dialog/confirm-account-dialog.component';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm
  hasError: boolean = false
  isLoading: boolean = false
  emailVerificationDialogRef: MatDialogRef<ConfirmAccountDialogComponent>



  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private tabService: TabService,
    private dialog: MatDialog
    ) { 
    this.signupForm = this.formBuilder.group({
      email: ["", emailValidator],
      password: ["", [Validators.required, Validators.minLength(8)]],
      confirmPassword: [undefined, passwordValidator('password')],
      firstName: "",
      surname: ""
    })
  }

  ngOnInit() {
  }

  onSubmit(userData){
    this.isLoading = true
    let config = new MatSnackBarConfig()
    config.duration = 3000
    this.userService.createUser(userData).subscribe(result => {
      this.tabService.setTabSelected(0);
      this.tabService.setEmail(userData.email)
      this.isLoading = true
      this.openResetPasswordDialog()
    },
      error => {
        if (error.status == 409) {
          this.hasError = true;
          this.snackBar.open("Couldn't create account. Email already taken", "", config)
        }
        if (error.status != 409) {
          this.hasError = true;
          this.snackBar.open("Couldn't create account. Something went wrong", "", config)
        }
        this.isLoading = false
      },
      () => {
        this.isLoading = false
      }
    )

  }
  openResetPasswordDialog() {
    this.emailVerificationDialogRef = this.dialog.open(ConfirmAccountDialogComponent)
  }

}
