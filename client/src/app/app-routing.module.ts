import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './components/home-page/home-page.component';
import { AuthenticationGuard } from './security/authentication.guard';
import { LoginSignupComponent } from './components/login-signup/login-signup.component';
import { RoleGuard } from './security/role.guard';
import { Roles } from './models/roles';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { NoauthGuard } from './security/noauth.guard';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { ConfirmAccountComponent } from './components/confirm-account/confirm-account.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';




const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginSignupComponent, canActivate: [NoauthGuard] },
  { path: 'confirm-account', component: ConfirmAccountComponent, canActivate: [NoauthGuard] },
  { path: 'reset-password/:token', component: ResetPasswordComponent, canActivate: [NoauthGuard] },
  { path: 'homepage', component: HomePageComponent, canActivate: [AuthenticationGuard] },
  { path: 'settings', component: UserSettingsComponent, canActivate: [AuthenticationGuard] },
  {
    path: 'admin', component: AdminPageComponent, canActivate: [RoleGuard],
    data: { expectedRole: [Roles.admin,] }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
