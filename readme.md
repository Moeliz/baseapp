# Base app

A web app with an Angular frontend, Spring Boot backend, MySQL database and Nginx as proxy.
Its purpose is to be the starting point for further development. 

## Features
#### Role_user:

Sign up with email verification

Login

Sign out

Forgot password feature

Update account info

Change password


#### Role_admin:

See all users

Delete user

Create new admin


## Tech/framework used
### Back end
A Spring Boot backend secured with Spring-security and JWT.
It is setup with two different roles, User and Admin.

### Front end
An Angular 8 front end with Material for styling.
The application is restricted based on the users role. See routing and navbar.

### Proxy
Nginx as a reverse proxy.

### Docker
Docker is used to build and start the project.

## Get started
First you need to set up an email for the email verification.
Set email and password in the application.properties file/files. 

`spring.mail.username = example@example.com`

`spring.mail.password = examplePassword`

### Production

#### Requirments
Docker 17.05 or higher. (multi-stage)

#### Build

`git clone https://gitlab.com/Moeliz/baseapp.git`

`cd baseapp`

`docker-compose up --build`

Client:
http://localhost/

Create a user or log in as admin:

Email: admin@admin.com

password: nimda

Api:
http://localhost/api


### Development

#### Requirments
MySQL
Maven
JDK 12
Node/npm

#### Start
`git clone https://gitlab.com/Moeliz/baseapp.git`

#### MySQL

Open the MySQL client in a terminal and create a new database and user.

`mysql> create database baseapp;` -- Creates the new database

`mysql> create user 'baseapp'@'%' identified by 'baseapppassword';` -- Creates the user

`mysql> grant all on baseapp.* to 'baseapp'@'%';` -- Gives all privileges to the new user on the newly created database

#### Spring Boot
The backend can be started with your IDE of choice or these commands in a terminal:

`cd backend`

`mvn spring-boot:run`

http://localhost:8080/api

#### Angular

Go to the client folder

`cd client`

`npm install`

`npm start`

Open your browser on http://localhost:4200/

Create a user or log in as admin:

Email: admin@admin.com

password: nimda






