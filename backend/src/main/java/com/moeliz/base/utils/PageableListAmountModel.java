package com.moeliz.base.utils;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class PageableListAmountModel {

    private List<Object> objects = new ArrayList<>();
    private Long amount;
}
