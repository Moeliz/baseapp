package com.moeliz.base.user.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class ConfirmationTokenException extends RuntimeException{
    /**
     *
     */
    private static final long serialVersionUID = -817002722340071079L;

    public ConfirmationTokenException(String errorMessage) {
        super(errorMessage);
    }

}