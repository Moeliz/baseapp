package com.moeliz.base.user;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class CustomizedUserRepositoryImpl implements CustomizedUserRepository {

    @Autowired
    private EntityManager entityManager;
    @Value("${database.name}")
    private String databaseName;

    @Override
    public List<User> searchNativeQuery(String sortby, String direction, int page, String searchedWords) {
        Query query = entityManager.createNativeQuery(createDynamicQuery(sortby, direction, page, searchedWords),
                User.class);
        query.setFirstResult(page * 10);
        query.setMaxResults(10);
        return query.getResultList();
    }

    @Override
    public Integer amountOfUsersNativeQuery(String sortby, String direction, int page, String searchedWords) {
        Query query = entityManager.createNativeQuery(createDynamicQuery(sortby, direction, page, searchedWords),
                User.class);
        return query.getResultList().size();
    }

    public String createDynamicQuery(String sortby, String direction, int page, String searchedWords) {
        StringBuffer query = new StringBuffer();
        String selectAllAndJoin = "SELECT * FROM " + databaseName + ".user u JOIN " + databaseName
                + ".user_roles ur ON (u.id = ur.user_id) WHERE is_enabled ";
        if (searchedWords.length() != 0) {
            String search = searchedWords;
            String[] splitted = search.split("\\s+");
            query.append(selectAllAndJoin + "AND MATCH(u.first_name,u.surname, u.email) ");
            for (int i = 0; i < splitted.length; i++) {

                if (i == 0) {
                    query.append("AGAINST('" + splitted[i] + "*' IN BOOLEAN MODE)");
                } else {
                    query.append(" AND MATCH(u.first_name,u.surname, u.email) AGAINST('" + splitted[i]
                            + "*' IN BOOLEAN MODE)");
                }
            }
        } else {
            query.append(selectAllAndJoin);
        }

        // Order by
        if (sortby.equals("roles")) {
            System.out.println("hejRoles");
            query.append("ORDER BY ur." + sortby);
        } else {
            query.append("ORDER BY u." + sortby);
        }

        // what direction
        query.append(" " + direction);

        System.out.println(query);
        return query.toString();
    }
}