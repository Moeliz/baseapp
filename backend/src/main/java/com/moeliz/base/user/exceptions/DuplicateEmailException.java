package com.moeliz.base.user.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DuplicateEmailException extends RuntimeException{
    /**
     *
     */
    private static final long serialVersionUID = -1192523754770415085L;

    public DuplicateEmailException(String errorMessage) {
        super(errorMessage);
    }

}