package com.moeliz.base.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.moeliz.base.user.exceptions.ConfirmationTokenException;
import com.moeliz.base.user.exceptions.DuplicateEmailException;
import com.moeliz.base.user.exceptions.UserNotFoundException;
import com.moeliz.base.utils.PageableListAmountModel;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import com.moeliz.base.email.EmailSenderService;
import com.moeliz.base.security.ConfirmationToken;
import com.moeliz.base.security.ConfirmationTokenRepository;
import com.moeliz.base.security.JwtTokenProvider;
import com.moeliz.base.security.TokenModel;
import com.moeliz.base.security.role.Role;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailSenderService emailSenderService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Value("${baseapp.client}")
    private String client;

    public UserService() {
    }

    public UserModel createAdmin(UserModel userModel) {
        User user = new User();
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ROLE_ADMIN);
        user.setEmail(userModel.getEmail());
        user.setFirstName(userModel.getFirstName());
        user.setSurname(userModel.getSurname());
        user.setPassword(passwordEncoder.encode(userModel.getPassword()));
        user.setRoles(roles);
        user.setEnabled(true);
        userRepository.save(user);
        return new UserModel(user);
    }

    public UserModel createUser(UserModel userModel) throws DuplicateEmailException {
        try {

            User user = new User();
            List<Role> roles = new ArrayList<>();
            roles.add(Role.ROLE_USER);
            user.setEmail(userModel.getEmail());
            user.setFirstName(userModel.getFirstName());
            user.setSurname(userModel.getSurname());
            user.setPassword(passwordEncoder.encode(userModel.getPassword()));
            user.setRoles(roles);
            userRepository.save(user);
            sendConfirmationEmail(user);

            return new UserModel(user);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateEmailException("Email already taken");
        }
    }

    public void sendConfirmationEmail(User user) {

        ConfirmationToken confirmationToken = new ConfirmationToken(user);

        confirmationTokenRepository.save(confirmationToken);

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setSubject("Complete Registration!");
        mailMessage.setText("To confirm your account, please click here : " + client + "/confirm-account?token="
                + confirmationToken.getConfirmationToken() + ".\n" + "Link expires in 24h");

        emailSenderService.sendEmail(mailMessage);

    }

    public UserModel getUserByEmail(String email) {
        User user = userRepository.findUserByEmail(email);
        return new UserModel(user);
    }

    public PageableListAmountModel getAllUsers(Pageable pageable) {
        Page<User> users = userRepository.findByIsEnabled(true, pageable);
        long amountOfUsers = userRepository.countByIsEnabled(true);
        List<Object> userModels = convertUserToUserModel(users);
        PageableListAmountModel pUsers = new PageableListAmountModel(userModels, amountOfUsers);
        return pUsers;
    }

    public ResponseEntity<Void> deleteUser(Long id) {
        User user = userRepository.getOne(id);
        if (user == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            userRepository.deleteById(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
    }

    public UserModel updateUserInfo(UserModel userModel, String email) {
        User user = userRepository.findUserByEmail(email);
        user.setEmail(userModel.getEmail());
        user.setFirstName(userModel.getFirstName());
        user.setSurname(userModel.getSurname());
        UserModel returnModel = new UserModel(userRepository.save(user));
        return returnModel;
    }

    public String updateUserInfoAndRefreshToken(UserModel user, String email)
            throws UserNotFoundException, DuplicateEmailException {
        try {
            isAuthenticated(email, user.getPassword());
            updateUserInfo(user, email);
            return login(user.getEmail(), user.getPassword());
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateEmailException("Email already taken");
        } catch (Exception e) {
            throw new UserNotFoundException("Bad Credentials");
        }
    }

    public UserModel updateUserPassword(UserModel userModel, String email) {
        User user = userRepository.findUserByEmail(email);
        user.setPassword(passwordEncoder.encode(userModel.getNewPassword()));
        return new UserModel(userRepository.save(user));
    }

    public String updatePasswordAndRefreshToken(UserModel userModel, String email) throws UserNotFoundException {
        try {
            isAuthenticated(email, userModel.getPassword());
            updateUserPassword(userModel, email);
            return login(email, userModel.getNewPassword());

        } catch (Exception e) {
            throw new UserNotFoundException("Bad Credentials");
        }

    }

    public UserModel getUserById(Long userId) {
        User user = userRepository.getOne(userId);
        return new UserModel(user);
    }

    public boolean isAuthenticated(String email, String password) {

        return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password))
                .isAuthenticated();
    }

    public String login(String email, String password) throws UserNotFoundException {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
            return jwtTokenProvider.createToken(email,
                    userRepository.findUserByEmailAndIsEnabled(email, true).getRoles());
        } catch (Exception e) {
            throw new UserNotFoundException("Bad Credentials");
        }
    }

    public Role[] getAllRoles() {
        Role[] roles = Role.class.getEnumConstants();
        return roles;
    }

    public List<Object> convertUserToUserModel(Page<User> users) {
        List<Object> userModels = new ArrayList<>();
        users.forEach(user -> {
            userModels.add(new UserModel(user));
        });

        return userModels;
    }

    public void confirmAccount(String token) throws ConfirmationTokenException {
        try {
            ConfirmationToken confirmationToken = confirmationTokenRepository.findByConfirmationToken(token);
            Instant confirmedTime = Instant.now();
            if (confirmedTime.isBefore(confirmationToken.getCreatedDate().plusSeconds(86400))) {
                User user = userRepository.findUserByEmail(confirmationToken.getUser().getEmail());
                user.setEnabled(true);
                userRepository.save(user);
                confirmationTokenRepository.delete(confirmationToken);
            } else {
                confirmationTokenRepository.delete(confirmationToken);
                throw new ConfirmationTokenException("Couldn't verify account. Invalid token");
            }
        } catch (Exception e) {
            throw new ConfirmationTokenException("Couldn't verify account. Invalid token");
        }

    }

    public void resetPassword(TokenModel tokenModel) throws ConfirmationTokenException {
        try {
            ConfirmationToken confirmationToken = confirmationTokenRepository
                    .findByConfirmationToken(tokenModel.getToken());
            Instant confirmedTime = Instant.now();
            System.out.println(confirmationToken.getCreatedDate());
            System.out.println(confirmedTime);
            if (confirmedTime.isBefore(confirmationToken.getCreatedDate().plusSeconds(86400))) {
                User user = userRepository.findUserByEmail(confirmationToken.getUser().getEmail());
                user.setPassword(passwordEncoder.encode(tokenModel.getPassword()));
                userRepository.save(user);
                confirmationTokenRepository.delete(confirmationToken);
            } else {
                confirmationTokenRepository.delete(confirmationToken);
                throw new ConfirmationTokenException("Couldn't reset password. Invalid token");
            }

        } catch (Exception e) {
            throw new ConfirmationTokenException("Couldn't reset password. Invalid token");
        }
    }

    public void sendResetPasswordEmail(UserModel userModel) {
        User user = userRepository.findUserByEmail(userModel.getEmail());
        ConfirmationToken confirmationToken = new ConfirmationToken(user);

        confirmationTokenRepository.save(confirmationToken);
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setSubject("Reset your password");
        mailMessage.setText("To reset your password, please follow the link: " + client + "/reset-password/"
                + confirmationToken.getConfirmationToken() + ".\n" + "Link expires in 24h");

        emailSenderService.sendEmail(mailMessage);

    }
}
