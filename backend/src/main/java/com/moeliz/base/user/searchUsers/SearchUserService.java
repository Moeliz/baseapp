package com.moeliz.base.user.searchUsers;

import java.util.ArrayList;
import java.util.List;

import com.moeliz.base.user.User;
import com.moeliz.base.user.UserModel;
import com.moeliz.base.user.UserRepository;
import com.moeliz.base.utils.PageableListAmountModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchUserService {

    @Autowired
    UserRepository userRepository;

    public PageableListAmountModel searchNativeQuery(String sortby, String direction, int page, String searchedWords) {
        List<User> users = userRepository.searchNativeQuery(sortby, direction, page, searchedWords);
        List<Object> userModels = convertUsersToUserModels(users);
        long amountOfUsers = userRepository.amountOfUsersNativeQuery(sortby, direction, page, searchedWords);
        PageableListAmountModel response = new PageableListAmountModel(userModels, amountOfUsers);
        return response;
    }

    public List<Object> convertUsersToUserModels(List<User> users) {
        List<Object> userModels = new ArrayList<>();
        users.forEach(user -> {
            userModels.add(new UserModel(user));
        });

        return userModels;
    }

}