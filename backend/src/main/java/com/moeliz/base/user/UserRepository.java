package com.moeliz.base.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.moeliz.base.security.role.Role;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, CustomizedUserRepository {

    User findUserByEmail(String email);
    List<User> findAllByRoles(Role role);
    Page<User> findByIsEnabled(boolean isEnabled, Pageable pageable);
    long countByIsEnabled(boolean isEnabled);
    User findUserByEmailAndIsEnabled(String email,boolean isEnabled);

}