package com.moeliz.base.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.moeliz.base.security.role.Role;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserModel {

    private Long id;
    private String firstName;
    private String surname;
    private String email;
    @JsonIgnore
    private String password;
    @JsonIgnore
    private String newPassword;
    private List<Role> roles = new ArrayList<>();
    @JsonIgnore
    private boolean isEnabled;

    public UserModel(User user){
        setId(user.getId());
        setFirstName(user.getFirstName());
        setSurname(user.getSurname());
        setEmail(user.getEmail());
        setPassword(user.getPassword());
        setRoles(user.getRoles());
        setEnabled(user.isEnabled());
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }
    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }
    @JsonIgnore
    public String getNewPassword() {
        return newPassword;
    }
    @JsonProperty
    public void setNewPassword(String password) {
        this.newPassword = password;
    }
    @JsonIgnore
    public boolean getEnabled() {
        return isEnabled;
    }
    @JsonProperty
    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

}
