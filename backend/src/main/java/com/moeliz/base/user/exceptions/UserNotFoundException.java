package com.moeliz.base.user.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException{
    /**
     *
     */
    private static final long serialVersionUID = -7556409859658444437L;

    public UserNotFoundException(String errorMessage) {
        super(errorMessage);
    }

}