package com.moeliz.base.user;

import java.util.List;

interface CustomizedUserRepository {

    List<User> searchNativeQuery(String sortby, String direction, int page, String searchedWords);

    Integer amountOfUsersNativeQuery(String sortby, String direction, int page, String searchedWords);
}