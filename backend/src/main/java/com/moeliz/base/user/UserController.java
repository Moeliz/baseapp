package com.moeliz.base.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

import com.moeliz.base.security.TokenModel;
import com.moeliz.base.security.role.Role;
import com.moeliz.base.user.exceptions.UserNotFoundException;
import com.moeliz.base.user.searchUsers.SearchUserService;

import org.springframework.security.access.prepost.PreAuthorize;
import com.moeliz.base.utils.*;

@RestController
@RequestMapping("/api/user")

public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private SearchUserService searchUserService;

    public UserController() {
    }

    @PostMapping("/createadmin")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<UserModel> createAdmin(@RequestBody UserModel userModel) {
        return new ResponseEntity<>(userService.createAdmin(userModel), HttpStatus.OK);
    }

    @PostMapping("/createuser")
    public ResponseEntity<UserModel> createUser(@RequestBody UserModel userModel) {
        return new ResponseEntity<>(userService.createUser(userModel), HttpStatus.OK);
    }

    @GetMapping("/confirm-account")
    public ResponseEntity<String> confirmAccount(@RequestParam("token") String token) {
        userService.confirmAccount(token);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<UserModel> getUserByEmail(Principal principal) {
        return new ResponseEntity<>(userService.getUserByEmail(principal.getName()), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<String> updateUserInfo(@RequestBody UserModel user, Principal principal) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, userService.updateUserInfoAndRefreshToken(user, principal.getName()));
        return new ResponseEntity<>(headers, HttpStatus.OK);
    }

    @PutMapping("/password")
    public ResponseEntity<UserModel> updateUserPassword(@RequestBody UserModel user, Principal principal) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, userService.updatePasswordAndRefreshToken(user, principal.getName()));
        return new ResponseEntity<>(headers, HttpStatus.OK);
    }

    @PostMapping("/resetpassword")
    public ResponseEntity<String> resetPassword(@RequestBody TokenModel tokenModel) {
        userService.resetPassword(tokenModel);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/resetpasswordemail")
    public ResponseEntity<String> sendResetPasswordEmail(@RequestBody UserModel userModel) {
        userService.sendResetPasswordEmail(userModel);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        return userService.deleteUser(id);
    }

    @GetMapping("/allusers/{sortby}/{direction}/{page}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<PageableListAmountModel> getAllUsers(@PathVariable String sortby,
            @PathVariable String direction, @PathVariable int page, @RequestParam String search) {
        return new ResponseEntity<>(searchUserService.searchNativeQuery(sortby, direction, page, search),
                HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody UserModel userModel) throws UserNotFoundException {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, userService.login(userModel.getEmail(), userModel.getPassword()));
        return new ResponseEntity<>(headers, HttpStatus.OK);
    }

    @GetMapping("/allroles")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Role[]> getAllRoles() {
        return new ResponseEntity<>(userService.getAllRoles(), HttpStatus.OK);
    }
}
