package com.moeliz.base.exceptions;


import com.moeliz.base.user.exceptions.ConfirmationTokenException;
import com.moeliz.base.user.exceptions.UserNotFoundException;
import com.moeliz.base.user.exceptions.DuplicateEmailException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {


    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ExceptionResponse> Exception(Exception exception) {
        ExceptionResponse er = new ExceptionResponse();
        er.setMessage(exception.getMessage());
        er.setStatus(HttpStatus.FORBIDDEN);
        return new ResponseEntity<ExceptionResponse>(er, HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler(value = UserNotFoundException.class)
    public ResponseEntity<ExceptionResponse> userNotFoundException(UserNotFoundException exception) {
        ExceptionResponse er = new ExceptionResponse();
        er.setMessage(exception.getMessage());
        er.setStatus(HttpStatus.NOT_FOUND);
        return new ResponseEntity<ExceptionResponse>(er, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ConfirmationTokenException.class)
    public ResponseEntity<ExceptionResponse> ConfirmationTokenException(ConfirmationTokenException exception) {
        ExceptionResponse er = new ExceptionResponse();
        er.setMessage(exception.getMessage());
        er.setStatus(HttpStatus.FORBIDDEN);
        return new ResponseEntity<ExceptionResponse>(er, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = DuplicateEmailException.class)
    public ResponseEntity<ExceptionResponse> DuplicateEmailException(DuplicateEmailException exception) {
        ExceptionResponse er = new ExceptionResponse();
        er.setMessage(exception.getMessage());
        er.setStatus(HttpStatus.CONFLICT);
        return new ResponseEntity<ExceptionResponse>(er, HttpStatus.CONFLICT);
    }


}
