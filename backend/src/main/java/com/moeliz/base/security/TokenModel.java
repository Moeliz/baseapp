package com.moeliz.base.security;

import lombok.*;


@Getter
@Setter
@ToString
@NoArgsConstructor
public class TokenModel {

    private String token;
    private String password;

    public TokenModel(String token, String password){
        setToken(token);
        setPassword(password);
    }
}