package com.moeliz.base.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.moeliz.base.user.UserRepository;
import com.moeliz.base.user.UserService;

@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = true;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        
        // if (userRepository.findUserByEmail("admin") == null) {
        //     UserModel user = new UserModel();
        //     user.setRoles(new ArrayList<>(Arrays.asList(Role.ROLE_ADMIN)));
        //     user.setEmail("admin");
        //     user.setPassword("nimda");
        //     user.setSurname("AdminSurname");
        //     user.setFirstName("AdminFirstName");
        //     userService.createAdmin(user);
        // }
    }
}