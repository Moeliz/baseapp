ALTER TABLE user ADD FULLTEXT(first_name, surname, email);


insert into user (id, first_name, surname, email, password, is_enabled) values (1, 'adminfirstname', 'adminsurname', 'admin@admin.com', '$2a$04$NWd7Y.6MTN7YOUMsYc7Piu0RNBCxz7r6Ppm2n/U30u14gVbfxG6Pq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (2, 'erik', 'anton', 'tgyer1@studiopress.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (3, 'erik', 'hercules', 'gharcus2@unesco.org', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (4, 'Johny', 'Didsbury', 'jdidsbury3@addthis.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (5, 'Cristionna', 'Boyen', 'cboyen4@ftc.gov', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (6, 'Sibeal', 'Balham', 'sbalham5@mozilla.org', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (7, 'Ginevra', 'Bartosek', 'gbartosek6@va.gov', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (8, 'Richardo', 'Matley', 'rmatley7@geocities.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (9, 'Westbrooke', 'Smullen', 'wsmullen8@over-blog.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (10, 'Alameda', 'Leicester', 'aleicester9@dailymotion.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (11, 'Al', 'Statham', 'astathama@woothemes.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (12, 'Dorise', 'Drayson', 'ddraysonb@cafepress.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (13, 'Kassi', 'Oxenden', 'koxendenc@shutterfly.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (14, 'Silvio', 'Leyman', 'sleymand@clickbank.net', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (15, 'Lebbie', 'Sizzey', 'lsizzeye@chicagotribune.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (16, 'Timmy', 'Wessell', 'twessellf@bloglines.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (17, 'Trix', 'Mangeney', 'tmangeneyg@yolasite.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (18, 'Ianthe', 'M''Quhan', 'imquhanh@bloglovin.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (19, 'Simona', 'Beckworth', 'sbeckworthi@photobucket.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (20, 'Bastien', 'Weinham', 'bweinhamj@naver.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (21, 'Diarmid', 'Bentjens', 'dbentjensk@irs.gov', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (22, 'Ula', 'Leveritt', 'uleverittl@patch.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (23, 'Lezley', 'Colbourn', 'lcolbournm@mapquest.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (24, 'Rosa', 'MacCartair', 'rmaccartairn@sourceforge.net', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (25, 'Sybilla', 'Bilbrook', 'sbilbrooko@dyndns.org', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (26, 'Aloin', 'Bradane', 'abradanep@engadget.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (27, 'Lucita', 'Danilyak', 'ldanilyakq@infoseek.co.jp', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (28, 'Waylon', 'Drennan', 'wdrennanr@myspace.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (29, 'Carlene', 'Simmonite', 'csimmonites@bloglovin.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (30, 'Godfrey', 'Bett', 'gbettt@sphinn.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (31, 'Gian', 'Barca', 'gbarcau@purevolume.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (32, 'Lana', 'Downgate', 'ldowngatev@marriott.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (33, 'Phillipp', 'Trew', 'ptreww@independent.co.uk', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (34, 'Brana', 'Vallow', 'bvallowx@altervista.org', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (35, 'Roderic', 'Olczyk', 'rolczyky@japanpost.jp', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (36, 'Bettina', 'Poles', 'bpolesz@home.pl', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (37, 'Zita', 'Stovell', 'zstovell10@cbc.ca', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (38, 'Danella', 'Colbertson', 'dcolbertson11@behance.net', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (39, 'Della', 'Stilgo', 'dstilgo12@archive.org', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (40, 'Reba', 'Bidnall', 'rbidnall13@va.gov', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (41, 'Penelopa', 'Quant', 'pquant14@wix.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (42, 'Dru', 'Corbally', 'dcorbally15@japanpost.jp', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (43, 'Maurene', 'Ruthven', 'mruthven16@msu.edu', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (44, 'Stu', 'Hibbart', 'shibbart17@hostgator.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (45, 'Maris', 'Swait', 'mswait18@spiegel.de', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (46, 'Reinaldo', 'Pulman', 'rpulman19@ask.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (47, 'Shawn', 'Yorath', 'syorath1a@cnbc.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (48, 'Adham', 'Eisikovitsh', 'aeisikovitsh1b@eventbrite.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (49, 'Karlen', 'Leppingwell', 'kleppingwell1c@nifty.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (50, 'Karena', 'Godfroy', 'kgodfroy1d@omniture.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (51, 'Noni', 'MacElroy', 'nmacelroy1e@google.cn', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (52, 'Joli', 'Weepers', 'jweepers1f@infoseek.co.jp', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (53, 'Chilton', 'Garside', 'cgarside1g@nifty.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (54, 'Starlene', 'Coultar', 'scoultar1h@netlog.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (55, 'Rosella', 'Hefford', 'rhefford1i@skype.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (56, 'Judith', 'Wheadon', 'jwheadon1j@booking.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (57, 'Sebastiano', 'Meineken', 'smeineken1k@macromedia.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (58, 'Nesta', 'Gallon', 'ngallon1l@wikia.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (59, 'Lonnard', 'Harnes', 'lharnes1m@multiply.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (60, 'Mariejeanne', 'Leggin', 'mleggin1n@istockphoto.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (61, 'Jobie', 'Trappe', 'jtrappe1o@mac.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (62, 'Munroe', 'Philipart', 'mphilipart1p@smugmug.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (63, 'Elysia', 'Rubinivitz', 'erubinivitz1q@guardian.co.uk', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (64, 'Rochell', 'Swaddle', 'rswaddle1r@engadget.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (65, 'Teena', 'Alstead', 'talstead1s@printfriendly.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (66, 'Thornie', 'Totterdill', 'ttotterdill1t@bing.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (67, 'Annecorinne', 'Chaikovski', 'achaikovski1u@canalblog.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (68, 'Gayla', 'Magee', 'gmagee1v@examiner.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (69, 'Rowan', 'Lebond', 'rlebond1w@washington.edu', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (70, 'Drucill', 'Gravenell', 'dgravenell1x@slashdot.org', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (71, 'Winn', 'Connechie', 'wconnechie1y@mysql.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (72, 'Leilah', 'Polamontayne', 'lpolamontayne1z@nifty.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (73, 'Sephira', 'Comport', 'scomport20@technorati.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (74, 'Portia', 'Rylett', 'prylett21@infoseek.co.jp', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (75, 'Neddie', 'Andriolli', 'nandriolli22@amazonaws.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (76, 'Misha', 'Daubeny', 'mdaubeny23@macromedia.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (77, 'Kendricks', 'Gallihawk', 'kgallihawk24@cbsnews.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (78, 'Opal', 'Gorce', 'ogorce25@google.pl', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (79, 'Salomon', 'Jamieson', 'sjamieson26@wikimedia.org', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (80, 'Evangelia', 'Salack', 'esalack27@answers.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (81, 'Holly-anne', 'Duxfield', 'hduxfield28@hp.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (82, 'Nerissa', 'MacAlpyne', 'nmacalpyne29@jiathis.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (83, 'Pembroke', 'Breckin', 'pbreckin2a@edublogs.org', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (84, 'Marcel', 'Burchett', 'mburchett2b@wikimedia.org', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (85, 'Sallie', 'Dignam', 'sdignam2c@samsung.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (86, 'Anthe', 'Seel', 'aseel2d@fc2.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (87, 'Carey', 'Farrow', 'cfarrow2e@sakura.ne.jp', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (88, 'Hollyanne', 'Gissop', 'hgissop2f@issuu.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (89, 'Theodora', 'Jimmes', 'tjimmes2g@opensource.org', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (90, 'Gayleen', 'Bartlet', 'gbartlet2h@miitbeian.gov.cn', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (91, 'Virge', 'Syne', 'vsyne2i@ed.gov', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (92, 'Sheba', 'Kobu', 'skobu2j@discovery.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (93, 'Gwennie', 'Peter', 'gpeter2k@businesswire.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (94, 'Sibby', 'Mauro', 'smauro2l@scribd.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (95, 'Dex', 'Hackley', 'dhackley2m@1688.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (96, 'Arvin', 'Donet', 'adonet2n@webnode.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (97, 'Alyce', 'Harbach', 'aharbach2o@java.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (98, 'Sumner', 'Sieghard', 'ssieghard2p@bbb.org', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (99, 'Ailey', 'Fewings', 'afewings2q@vistaprint.com', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);
insert into user (id, first_name, surname, email, password, is_enabled) values (100, 'Aurlie', 'Fallawe', 'afallawe2r@w3.org', '$2a$04$Jbb8kyZf7jcmlgLql5vsou/dI24ATj06ry9U/eSpRFIARjYWnNSZq', true);

insert into user_roles (user_id, roles) values (1, '0');
insert into user_roles (user_id, roles) values (2, '1');
insert into user_roles (user_id, roles) values (3, '1');
insert into user_roles (user_id, roles) values (4, '1');
insert into user_roles (user_id, roles) values (5, '1');
insert into user_roles (user_id, roles) values (6, '1');
insert into user_roles (user_id, roles) values (7, '1');
insert into user_roles (user_id, roles) values (8, '1');
insert into user_roles (user_id, roles) values (9, '1');
insert into user_roles (user_id, roles) values (10, '1');
insert into user_roles (user_id, roles) values (11, '1');
insert into user_roles (user_id, roles) values (12, '1');
insert into user_roles (user_id, roles) values (13, '1');
insert into user_roles (user_id, roles) values (14, '1');
insert into user_roles (user_id, roles) values (15, '1');
insert into user_roles (user_id, roles) values (16, '1');
insert into user_roles (user_id, roles) values (17, '1');
insert into user_roles (user_id, roles) values (18, '1');
insert into user_roles (user_id, roles) values (19, '1');
insert into user_roles (user_id, roles) values (20, '1');
insert into user_roles (user_id, roles) values (21, '1');
insert into user_roles (user_id, roles) values (22, '1');
insert into user_roles (user_id, roles) values (23, '1');
insert into user_roles (user_id, roles) values (24, '1');
insert into user_roles (user_id, roles) values (25, '1');
insert into user_roles (user_id, roles) values (26, '1');
insert into user_roles (user_id, roles) values (27, '1');
insert into user_roles (user_id, roles) values (28, '1');
insert into user_roles (user_id, roles) values (29, '1');
insert into user_roles (user_id, roles) values (30, '1');
insert into user_roles (user_id, roles) values (31, '1');
insert into user_roles (user_id, roles) values (32, '1');
insert into user_roles (user_id, roles) values (33, '1');
insert into user_roles (user_id, roles) values (34, '1');
insert into user_roles (user_id, roles) values (35, '1');
insert into user_roles (user_id, roles) values (36, '1');
insert into user_roles (user_id, roles) values (37, '1');
insert into user_roles (user_id, roles) values (38, '1');
insert into user_roles (user_id, roles) values (39, '1');
insert into user_roles (user_id, roles) values (40, '1');
insert into user_roles (user_id, roles) values (41, '1');
insert into user_roles (user_id, roles) values (42, '1');
insert into user_roles (user_id, roles) values (43, '1');
insert into user_roles (user_id, roles) values (44, '1');
insert into user_roles (user_id, roles) values (45, '1');
insert into user_roles (user_id, roles) values (46, '1');
insert into user_roles (user_id, roles) values (47, '1');
insert into user_roles (user_id, roles) values (48, '1');
insert into user_roles (user_id, roles) values (49, '1');
insert into user_roles (user_id, roles) values (50, '1');
insert into user_roles (user_id, roles) values (51, '1');
insert into user_roles (user_id, roles) values (52, '1');
insert into user_roles (user_id, roles) values (53, '1');
insert into user_roles (user_id, roles) values (54, '1');
insert into user_roles (user_id, roles) values (55, '1');
insert into user_roles (user_id, roles) values (56, '1');
insert into user_roles (user_id, roles) values (57, '1');
insert into user_roles (user_id, roles) values (58, '1');
insert into user_roles (user_id, roles) values (59, '1');
insert into user_roles (user_id, roles) values (60, '1');
insert into user_roles (user_id, roles) values (61, '1');
insert into user_roles (user_id, roles) values (62, '1');
insert into user_roles (user_id, roles) values (63, '1');
insert into user_roles (user_id, roles) values (64, '1');
insert into user_roles (user_id, roles) values (65, '1');
insert into user_roles (user_id, roles) values (66, '1');
insert into user_roles (user_id, roles) values (67, '1');
insert into user_roles (user_id, roles) values (68, '1');
insert into user_roles (user_id, roles) values (69, '1');
insert into user_roles (user_id, roles) values (70, '1');
insert into user_roles (user_id, roles) values (71, '1');
insert into user_roles (user_id, roles) values (72, '1');
insert into user_roles (user_id, roles) values (73, '1');
insert into user_roles (user_id, roles) values (74, '1');
insert into user_roles (user_id, roles) values (75, '1');
insert into user_roles (user_id, roles) values (76, '1');
insert into user_roles (user_id, roles) values (77, '1');
insert into user_roles (user_id, roles) values (78, '1');
insert into user_roles (user_id, roles) values (79, '1');
insert into user_roles (user_id, roles) values (80, '1');
insert into user_roles (user_id, roles) values (81, '1');
insert into user_roles (user_id, roles) values (82, '1');
insert into user_roles (user_id, roles) values (83, '1');
insert into user_roles (user_id, roles) values (84, '1');
insert into user_roles (user_id, roles) values (85, '1');
insert into user_roles (user_id, roles) values (86, '1');
insert into user_roles (user_id, roles) values (87, '1');
insert into user_roles (user_id, roles) values (88, '1');
insert into user_roles (user_id, roles) values (89, '1');
insert into user_roles (user_id, roles) values (90, '1');
insert into user_roles (user_id, roles) values (91, '1');
insert into user_roles (user_id, roles) values (92, '1');
insert into user_roles (user_id, roles) values (93, '1');
insert into user_roles (user_id, roles) values (94, '1');
insert into user_roles (user_id, roles) values (95, '1');
insert into user_roles (user_id, roles) values (96, '1');
insert into user_roles (user_id, roles) values (97, '1');
insert into user_roles (user_id, roles) values (98, '1');
insert into user_roles (user_id, roles) values (99, '1');
insert into user_roles (user_id, roles) values (100, '1');
