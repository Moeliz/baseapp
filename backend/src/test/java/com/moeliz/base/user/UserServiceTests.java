package com.moeliz.base.user;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;

import java.util.ArrayList;
import java.util.List;

import com.moeliz.base.security.role.Role;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTests{

    @MockBean
    private UserRepository userRepository;

    @SpyBean
    private UserService userService;

    @MockBean
    private BCryptPasswordEncoder passwordEncoder;

    

    @Test
    public void shouldCreateAdmin(){
        User user = new User();

        Mockito.when(userRepository.save(user)).thenReturn(user);
        Mockito.when(passwordEncoder.encode("password")).thenReturn("password");

        List<Role> roles = new ArrayList<>();
        roles.add(Role.ROLE_ADMIN);

        UserModel userModel = UserModel.builder().email("Baseapp@app.com").password("password").roles(roles).build();
        UserModel userEqual = UserModel.builder().email("Baseapp@app.com").password("password").isEnabled(true).roles(roles).build();

        assertThat(userService.createAdmin(userModel)).isEqualToComparingFieldByField(userEqual);
    }

    @Test
    public void shouldCreateUser(){
        User user = new User();

        Mockito.when(userRepository.save(user)).thenReturn(user);
        Mockito.when(passwordEncoder.encode("password")).thenReturn("password");
        doNothing().when(userService).sendConfirmationEmail(isA(User.class));

        List<Role> roles = new ArrayList<>();
        roles.add(Role.ROLE_USER);

        UserModel userModel = UserModel.builder().email("Baseapp@app.com").password("password").roles(roles).build();
        UserModel userEqual = UserModel.builder().email("Baseapp@app.com").password("password").isEnabled(false).roles(roles).build();

        assertThat(userService.createUser(userModel)).isEqualToComparingFieldByField(userEqual);
    }
}